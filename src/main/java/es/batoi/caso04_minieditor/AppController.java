package es.batoi.caso04_minieditor;

import java.io.BufferedReader;
import java.io.BufferedWriter;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.prefs.Preferences;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Javi
 */
public class AppController implements Initializable {

	static final String KKeyLastDocument = "lastDocument";

	@FXML
	private TextArea taEditor;
	@FXML
	private Label lblInfo;
	@FXML
	private Button btnAbrir;
	@FXML
	private Button btnCerrar;
	@FXML
	private Button btnGuardar;
	@FXML
	private Button btnNuevo;
	@FXML
	private ComboBox<String> filesRecents;

	private Stage escenario;
	private File f;
	private Preferences pf;
	private boolean isSaved;
	private Alert alert;
	private String[] filesRecentsArray;

	/**
	 * Initializes the controller class.
	 */
	@Override
	public void initialize(URL url, ResourceBundle rb) {
		// Inicializaciones del controlador
		this.pf = Preferences.userRoot().node("miniEditorDeTexto");
		this.alert = new Alert(AlertType.CONFIRMATION);
		this.filesRecentsArray = new String[5];

		try {
			this.configurarEditor();
			this.establcerArchivosRecientes();
		} catch (IOException e) {
			e.printStackTrace();
		}

		this.isSaved = true;

	}

	private void configurarEditor() throws IOException {
		this.taEditor.setDisable(false); // HABILITAR EDICION DEL TEXT AREA

		// CONFIGURAMOS EL ALERT
		alert.setTitle("¡ATENCIÓN!");
		alert.setHeaderText("Estás apunto de cerrar el programa.");
		alert.setContentText("¿Quieres guardar los cambios antes de cerrar el programa?");
		ButtonType buttonTypeOne = new ButtonType("Guardar y salir");
		ButtonType buttonTypeTwo = new ButtonType("Salir");
		alert.getButtonTypes().setAll(buttonTypeOne, buttonTypeTwo);
		this.filesRecents.setValue("Archivos recientes");

		// AÑADIMOS LISTENER PARA SABER CUANDO ESCRIBIMOS Y MODIFICAR LA INFO DEL
		// PROGRAMA
		this.taEditor.textProperty().addListener((obs, old, niu) -> {
			try {
				this.establecerInfo();
				this.isSaved = false;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		});

		//AÑADIMOS LISTENER A ARCHIVOS RECIENTES
		this.filesRecents.getSelectionModel().selectedItemProperty().addListener((options, oldValue, ruta) -> {
			try {
				if (ruta != null){
					this.leerFichero(new File(ruta).toPath());
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		});

		if (this.ultimoArchivoUsado() != null) { // SI YA HEMOS USADO ANTERIORMENTE EL EDITOR
			this.leerFichero(this.f.toPath());
		}

		this.establecerInfo();

	}

	@FXML
	private void handleNuevo() {
		this.taEditor.setText(""); // REESTABLECEMOS EL TEXT AREA
		this.pf.remove(KKeyLastDocument); // QUITAMOS LA RUTA QUE INICIA AL PRINCIPIO
		this.isSaved = false;
	}

	@FXML
	private void handleAbrir() throws IOException {
		FileChooser fc = new FileChooser();
		fc.setTitle("ABRIR ARCHIVO");
		f = fc.showOpenDialog(escenario);

		if (f != null) { // COMPROBAMOS QUE NO HAYAMOS DADO A CANCELAR
			this.leerFichero(f.toPath());
			this.establecerInfo();
			this.guardarRuta(f.toString());
			this.guardarArchivosRecientes(f.toString());
		}


	}

	@FXML
	private void handleGuardar() throws IOException {
		FileChooser fc = new FileChooser();
		fc.setTitle("GUARDAR ARCHIVO");
		fc.getExtensionFilters().addAll(new ExtensionFilter("Archivos de texto", "*.txt"));
		f = fc.showSaveDialog(this.escenario);

		if (f != null) { // COMPROBAMOS QUE NO HAYAMOS DADO A CANCELAR
			this.guardarFichero(this.f.toPath());
		}

	}

	@FXML
	private void handleCerrar() throws IOException {

		// SI NO ESTÁ GUARDADO MOSTRAMOS UN DIALOGO AL USUARIO PARA GUARDAR EL ARCHIVO
		// SI EL QUIERE
		if (!this.isSaved) {

			Optional<ButtonType> result = alert.showAndWait();
			if (result.get() == new ButtonType("Guardar y salir")) {
				FileChooser fc = new FileChooser();
				fc.setTitle("GUARDAR ARCHIVO");
				fc.getExtensionFilters().addAll(new ExtensionFilter("Archivos de texto", "*.txt"));
				f = fc.showSaveDialog(this.escenario);

				if (f != null) { // COMPROBAMOS QUE NO HAYAMOS DADO A CANCELAR
					this.guardarFichero(this.f.toPath());
				}
			}

		}

		System.exit(0);
	}

	void setEscenario(Stage escenario) {
		this.escenario = escenario;
	}

	public void establecerInfo() throws IOException {
		// ESTA FUNCION LEE EL TEXTO DEL TEXT AREA Y COMPRUEBA CUANTOS CARÁCTERES Y
		// CUANTAS LINEAS HAY
		// Y DESPUES LAS INSERTA EN EL LABEL CORRESPONDIENTE

		String[] lineas = this.taEditor.getText().split("\n");
		int numLineas = lineas.length;
		int caracteres = 0;

		for (int i = 0; i < numLineas; i++) {
			caracteres += lineas[i].length();
		}

		this.lblInfo.setText("Líneas: " + numLineas + " - Carácteres: " + caracteres);
	}

	public void guardarRuta(String ruta) {
		// ESTA FUNCION GUARDA LA UTLIMA RUTA QUE SE ABRE
		this.pf.put(KKeyLastDocument, ruta);
	}

	public String ultimoArchivoUsado() {
		// ESTA FUNCION DEVUELVE LA ÚLTIMA RUTA QUE SE ABRE
		String ruta=this.pf.get(KKeyLastDocument, null);

		if (ruta != null){
			this.f = new File(ruta);
		}

		return ruta;
	}

	public void establcerArchivosRecientes(){
		//VOLCAMOS TODOS LOS FICHEROS RECIENTES EN EL COMPONENTE

		//COGEMOS LOS FICHEROS RECIENTES GUARDADOS Y LOS INSERTAMOS EN EL ARRAY
		for (int i=0; i<this.filesRecentsArray.length;i++) {
			this.filesRecentsArray[i]=this.pf.get(KKeyLastDocument + "-" + i, null);
		}

		//BORRAMOS TODOS LOS ARCHIVOS RECIENTES
		this.filesRecents.getItems().clear();

		for (String s : this.filesRecentsArray) {

			if (s != null) {
				//SI EXISTE RUTA LA AÑADIMOS EN EL COMBO BOX
				this.filesRecents.getItems().add(s);
			}

		}

	}

	public void guardarArchivosRecientes(String ruta){
		//ARRAY AUXILIAR
		String[] aux = new String[this.filesRecentsArray.length];


		//ORDENAMOS EL ARRAY CON EL NUEVO FICHERO
		for (int i=1; i<this.filesRecentsArray.length;i++) {
			aux[i]=this.filesRecentsArray[i-1];
		}
		aux[0]=ruta;
		this.filesRecentsArray=aux;


		//GUARDAMOS EL ARRAY
		for (int i=0; i<this.filesRecentsArray.length;i++){
			System.out.println( this.filesRecentsArray[i]);

			if (this.filesRecentsArray[i] != null){
				this.pf.put(KKeyLastDocument + "-" + i, this.filesRecentsArray[i]); //GUARDAMOS LOS OTROS
			}
		}

		this.establcerArchivosRecientes();

	}

	public String leerFichero(Path archivo) throws IOException {
		// ESTA FUNCION RECIBE UN PARAMETRO ARCHIVO (TIPO PATH)
		// LO LEE Y LO INSERTA EN EL TEXTAREA

		BufferedReader br = Files.newBufferedReader(archivo);
		String lin = null;
		String aux = "";
		while ((lin = br.readLine()) != null) {
			aux += lin + "\n";
		}
		
		br.close();

		this.taEditor.setText(aux);


		return aux;
	}

	public void guardarFichero(Path archivo) throws IOException {
		// ESTA FUNCION GUARDA EL FICHERO

		BufferedWriter bw = Files.newBufferedWriter(archivo);
		String[] lineas = this.taEditor.getText().split("\n");

		// RECORREMOS PARA GUARDAR CADA LINEA
		for (int i = 0; i < lineas.length; i++) {
			bw.write(lineas[i]);
			bw.newLine();
		}

		bw.close();
		
		//CAMBIAMOS LA ULTIMA RUTA
		this.guardarRuta(archivo.toString());
		
		//MARCAMOS EL FICHERO COMO GUARDADO
		this.isSaved = true;

	}

}
